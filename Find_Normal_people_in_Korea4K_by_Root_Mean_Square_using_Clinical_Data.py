# Jeongwoo Seo 
# 2022-04-06

from re import T
import pandas as pd
import numpy as np
import math

def read_traits(path_of_selected_traits_input_txt_file):
    traits_list = []
    with open(path_of_selected_traits_input_txt_file, 'r') as traits:
        for line in traits:
            line = line.replace('\n','')
            if line != '':
                traits_list.append(line)
    print(f"=== {len(traits_list)} Traits Selected ===")    
    return traits_list
    

def read_clinical_data(clinical_data_path):
    clinical_data = pd.read_csv(clinical_data_path, sep='\t', index_col= ['FID','IID'])
    return clinical_data

def calculate_root_mean_square(traits_list,clinical_data, option):
    # if option is 1: use only root mean square
    # if option is 2: use root mean square and clinical data
    
    for idx in clinical_data.index:
        square_list = []
        skipped_because_of_NA_count = 0
        trait_num = 0    
        for col in clinical_data.columns:
            for selected_trait in traits_list:
                if col == selected_trait:
                    if clinical_data.loc[idx,col] == clinical_data.loc[idx,col]:
                        square = math.pow(float(clinical_data.loc[idx,col]),2)
                        square_list.append(square)
                        trait_num += 1
                    else:
                        skipped_because_of_NA_count += 1
        if trait_num != 0:
            rootmeansquare = math.sqrt((sum(square_list)/trait_num))
            clinical_data.loc[idx,'Root_Mean_Square'] = rootmeansquare
            clinical_data.loc[idx,f'NA_count_among_{len(traits_list)}_traits'] = int(skipped_because_of_NA_count)
        elif trait_num == int(0):
            clinical_data.loc[idx,'Root_Mean_Square'] = -9
            clinical_data.loc[idx,f'NA_count_among_{len(traits_list)}_traits'] = int(skipped_because_of_NA_count)
        else:
            print(f"=== {idx} Unexpected Error ===")
            clinical_data.loc[idx,'Root_Mean_Square'] = -8
            clinical_data.loc[idx,f'NA_count_among_{len(traits_list)}_traits'] = int(skipped_because_of_NA_count)
    
    
    if option == 1:
        selected_total_columns = ['Sex','Age','Root_Mean_Square',f'NA_count_among_{len(traits_list)}_traits']
    if option == 2:
        selected_total_columns = ['Sex','Age','Root_Mean_Square',f'NA_count_among_{len(traits_list)}_traits'] + traits_list
    return clinical_data[selected_total_columns]


def sorting_calculated_data(dataframe_after_calculation,sorting_standard_column):
    dataframe_after_calculation = dataframe_after_calculation.sort_values(by=sorting_standard_column,ascending=False)
    return dataframe_after_calculation



def main():
    traits_to_run_root_mean_square = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Analysis/Healthome/Find_normal_people_in_Korea4K_by_Root_Mean_Square_using_Clinical Data/Scripts/Selected_traits.txt'
    clinical_data_path = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Resources/ClinicalInformation_2022/KU10K_CI_korea4K_ver1.6_original_recent_normalized_4k.txt'
    
    data = read_clinical_data(clinical_data_path)
    traits = read_traits(traits_to_run_root_mean_square)
    option = 1
    end_dataframe = calculate_root_mean_square(traits,data,option)
    end_dataframe = sorting_calculated_data(end_dataframe,'Root_Mean_Square')
    
    if option == 1:
        endfile_path = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Analysis/Healthome/Find_normal_people_in_Korea4K_by_Root_Mean_Square_using_Clinical Data/Results/Root_mean_square_table.txt'
    if option == 2:
        endfile_path ='/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Analysis/Healthome/Find_normal_people_in_Korea4K_by_Root_Mean_Square_using_Clinical Data/Results/Root_mean_square_table_with_clinical_data.txt'

    end_dataframe.to_csv(endfile_path,sep='\t')
    print('=== END ===')
    
if __name__ == "__main__":
	main()