# Jeongwoo Seo
# 2022-04-13
import pandas as pd
from Find_Normal_people_in_Korea4K_by_Root_Mean_Square_using_Clinical_Data import read_traits




def merge_unnormalized_data_to_root_mean_square_result(root_mean_square_result_dataframe, unnormalized_dataframe, traitlist):
    unnormalized_dataframe = unnormalized_dataframe[traitlist]
    merged = pd.merge(root_mean_square_result_dataframe,unnormalized_dataframe, left_index=True,right_index=True)
    return merged

def edit_codebook_dataframe_columns(codebook_dataframe):
    codebook_dataframe = codebook_dataframe[['Item', 'Normal_range']]
    codebook_dataframe = codebook_dataframe.astype(str)
    return codebook_dataframe

def convert_unnormalized_value_to_normal_abnormal_binary(merged_unn_rms_dataframe, codebook_dataframe, traitlist):
    codebook_edited = edit_codebook_dataframe_columns(codebook_dataframe)
    for idx in merged_unn_rms_dataframe.index:
        for col in merged_unn_rms_dataframe.columns:
            if col in traitlist:
                for idx2 in codebook_edited.index:
                    if col == codebook_edited.loc[idx2,'Item']:
                        normal_range = codebook_edited.loc[idx2,'Normal_range']
                        normal_range = normal_range.split('~')
                        if len(normal_range) == 2 : 
                            normal_range = list(map(float,normal_range))
                            if float(merged_unn_rms_dataframe.loc[idx,col]) >= normal_range[0] and float(merged_unn_rms_dataframe.loc[idx,col]) <= normal_range[1]:
                                merged_unn_rms_dataframe.loc[idx,col] = 1
                            elif float(merged_unn_rms_dataframe.loc[idx,col]) <= normal_range[0] or float(merged_unn_rms_dataframe.loc[idx,col]) >= normal_range[1]:
                                merged_unn_rms_dataframe.loc[idx,col] = 0
    return merged_unn_rms_dataframe

def insert_length_column_in_converted_data(converted_dataframe,traitlist):
    for idx in converted_dataframe.index:
        count_one = 0
        count_zero = 0
        for col in converted_dataframe.col:
            if col in traitlist:
                if converted_dataframe.loc[idx,col] == converted_dataframe.loc[idx,col]:
                    if converted_dataframe.loc[idx,col] == 0:
                        count_zero += 1
                    elif converted_dataframe.loc[idx,col] == 1:
                        count_one += 1

        converted_dataframe.loc[idx,'Num_of_normal_ranges'] = count_one
        converted_dataframe.loc[idx,'Ratio_of_normal_ranges'] = count_one / (count_one + count_zero)
    column_arranged = ['FID','IID','Sex','Age','Root_Mean_Square','NA_count_among_80_traits','Num_of_normal_ranges','Ratio_of_normal_ranges'] + traitlist
    converted_dataframe = converted_dataframe[column_arranged]
    return converted_dataframe


def main():
    clinical_not_normalized = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Resources/ClinicalInformation_2022/KU10K_CI_korea4K_ver1.6_original_recent.txt'
    root_mean_square_result = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Analysis/Healthome/Find_normal_people_in_Korea4K_by_Root_Mean_Square_using_Clinical Data/Results/Root_mean_square_table.txt'
    codebook = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Resources/ClinicalInformation_2022/Codebook/All_Codebook_Eng_Clinical_Data_Integrated_with_Lifestyle_Ver1.6.xlsx'
    selected_traits = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Analysis/Healthome/Find_normal_people_in_Korea4K_by_Root_Mean_Square_using_Clinical Data/Scripts/Selected_traits.txt'
    endfile = '/BiO/Research/Project2/KOGIC-KU10K-Genome-2019-01/Analysis/Healthome/Find_normal_people_in_Korea4K_by_Root_Mean_Square_using_Clinical Data/Results/Check_if_the_trait_is_in_the_normal_range.txt'
    
    unnormali = pd.read_csv(clinical_not_normalized,sep='\t',encoding='unicode_escape',index_col= ['FID','IID'])
    rms = pd.read_csv(root_mean_square_result,sep='\t',encoding='unicode_escape',index_col= ['FID','IID'])
    codebook = pd.read_excel(codebook, engine = "openpyxl")
    traitlist = read_traits(selected_traits)
    merged = merge_unnormalized_data_to_root_mean_square_result(rms, unnormali, traitlist)
    codebook = edit_codebook_dataframe_columns(codebook)
    converted = convert_unnormalized_value_to_normal_abnormal_binary(merged, codebook, traitlist)
    final = insert_length_column_in_converted_data(converted,traitlist)

    final.to_csv(endfile,sep='\t',encoding='utf-8-sig')

if __name__ == "__main__":
    main()